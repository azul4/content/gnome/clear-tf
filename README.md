# clear-tf

For GNOME only. Delete the preview files from the existing ***~/.cache/thumbnails/fail/gnome-thumbnail-factory/*** folder. Just run from terminal (**don't use sudo**):

**clear-tf**

<br><br>

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/gnome/clear-tf.git
```
